var MongoClient = require('mongodb').MongoClient;

MongoClient.connect('mongodb://localhost:27017/school', function(err,db) {
	var students = db.collection('students');
	var cursor = students.find();

	var min = 0,
		minIndex = 0,
		doIt = true,
		arr = [],
		len = 0;

	cursor.each( function(err,student) {
		if (err) throw err;
		if (student === null) { return db.close() ; }


		console.log('in');
		min = 1000.00 ;
		doIt = false;
		arr = student.scores;
		len = student.scores.length;

		for (var i=0 ; i<len ; i++) {
			if (student.scores[i].type === 'homework') {
				if (parseFloat(student.scores[i].score) < min) {
					min = parseFloat(student.scores[i].score) ;
					minIndex = i;
					doIt = true;
				}
			}
		}

		if (doIt) {
			console.log(arr);
			arr.splice(minIndex,1);
			console.log(arr);
			students.update({_id: student._id}, {$set: {scores: arr}}, function(err, result){
				if (err) {throw err ;}
				// db.close();
			});
		}

	});
});