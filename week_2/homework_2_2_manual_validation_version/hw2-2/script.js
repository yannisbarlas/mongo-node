var MongoClient = require('mongodb').MongoClient;

MongoClient.connect('mongodb://localhost:27017/weather', function(err,db) {
	if (err) throw err;

	var data = db.collection('data');

	var options = {'sort': [ ['State', 1] , ['Temperature', -1] ]};

	var cursor = data.find({},{}, options);

	var previousState = '';

	cursor.each(function(err, item) {
		if (err) throw err;
		if (item === null) { return db.close() ; }

		if (item.State !== previousState ) {
			previousState = item.State;
			console.log(item.State + "  " + item.Temperature + "  " + item.month_high);
			// data.update({'_id': item._id}, {$set: {'month_high': true}}, function(err){});
		}

	});
});