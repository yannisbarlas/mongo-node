var MongoClient = require('mongodb').MongoClient;

MongoClient.connect('mongodb://localhost:27017/test', function(err, db) 
{
     if(err) throw err;

     var query = { 'grade' : 100};

     function callback(err, doc) {
          if(err) throw err;

          console.dir(doc);

          db.close();
     } 
     /* TODO */

     // console.log(db.collection("grades").find(query));
     db.collection("grades").findOne(query, callback);
     // console.log(db.collection("grades").findOne(query));

});